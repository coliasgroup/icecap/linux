#pragma once

#include <linux/kernel.h>
#include <linux/uaccess.h>

#include "sel4.h"

#define ICECAP_RING_BUFFER_CTRL_SIZE 0x1000

#define ICECAP_RING_BUFFER_S_NOTIFY_READ 0
#define ICECAP_RING_BUFFER_S_NOTIFY_WRITE 1

#define ICECAP_RING_BUFFER_NO_PACKET 0
#define ICECAP_RING_BUFFER_PACKET 1

typedef uint32_t icecap_ring_buffer_packet_header_t;

typedef struct icecap_ring_buffer_ctrl {
	size_t offset_r;
	size_t offset_w;
	uint64_t status;
} icecap_ring_buffer_ctrl_t;

typedef struct icecap_ring_buffer_side {
	size_t size;
	icecap_ring_buffer_ctrl_t *ctrl;
	char *buf;
} icecap_ring_buffer_side_t;

typedef enum {
	ICECAP_RING_BUFFER_KICK_TYPE_UNMANAGED,
	ICECAP_RING_BUFFER_KICK_TYPE_MANAGED
} icecap_ring_buffer_kick_type_t;

// HACK
#define ICECAP_RING_BUFFER_MAX_NUM_NODES 3

typedef struct {
	icecap_ring_buffer_kick_type_t type;
	union {
		struct {
			uint64_t notification;
		} unmanaged;
		struct {
			uint64_t endpoints[ICECAP_RING_BUFFER_MAX_NUM_NODES];
			uint64_t message;
		} managed;
	} value;
} icecap_ring_buffer_kick_t;

typedef struct icecap_ring_buffer {
	icecap_ring_buffer_side_t read;
	icecap_ring_buffer_side_t write;
	size_t private_offset_r;
	size_t private_offset_w;
	icecap_ring_buffer_kick_t kick;
} icecap_ring_buffer_t;

void icecap_ring_buffer_kick(icecap_ring_buffer_kick_t *kick);

size_t icecap_ring_buffer_poll_read(icecap_ring_buffer_t *b);
size_t icecap_ring_buffer_poll_write(icecap_ring_buffer_t *b);
void icecap_ring_buffer_read(icecap_ring_buffer_t *b, size_t n, char *buf);
void icecap_ring_buffer_read_to_user(icecap_ring_buffer_t *b, size_t n, char __user *buf);
void icecap_ring_buffer_skip(icecap_ring_buffer_t *b, size_t n);
void icecap_ring_buffer_peek(icecap_ring_buffer_t *b, size_t n, char *buf);
void icecap_ring_buffer_peek_to_user(icecap_ring_buffer_t *b, size_t n, char __user *buf);
void icecap_ring_buffer_write(icecap_ring_buffer_t *b, size_t n, const char *buf);
void icecap_ring_buffer_write_from_user(icecap_ring_buffer_t *b, size_t n, const char __user *buf);

int icecap_ring_buffer_poll_read_packet(icecap_ring_buffer_t *b, size_t *n);
int icecap_ring_buffer_poll_write_packet(icecap_ring_buffer_t *b, size_t n);
void icecap_ring_buffer_read_packet(icecap_ring_buffer_t *b, size_t n, char *buf);
void icecap_ring_buffer_write_packet(icecap_ring_buffer_t *b, size_t n, const char *buf);

void icecap_ring_buffer_notify_read(icecap_ring_buffer_t *b);
void icecap_ring_buffer_notify_write(icecap_ring_buffer_t *b);
void icecap_ring_buffer_enable_notify_read(icecap_ring_buffer_t *b);
void icecap_ring_buffer_enable_notify_write(icecap_ring_buffer_t *b);
void icecap_ring_buffer_disable_notify_read(icecap_ring_buffer_t *b);
void icecap_ring_buffer_disable_notify_write(icecap_ring_buffer_t *b);

void icecap_ring_buffer_init(icecap_ring_buffer_t *b);

void icecap_ring_buffer_of(struct platform_device *dev, icecap_ring_buffer_t *b);
