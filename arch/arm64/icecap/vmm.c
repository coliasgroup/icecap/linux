#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/debugfs.h>
#include <uapi/linux/icecap.h>

#include "sel4.h"
#include "vmm.h"

static struct dentry *icecap_vmm_file;

static void icecap_vmm_hvc(unsigned long sys_id, unsigned long *regs)
{
	register unsigned long int x0_ asm("x0") = regs[0];
	register unsigned long int x1_ asm("x1") = regs[1];
	register unsigned long int x2_ asm("x2") = regs[2];
	register unsigned long int x3_ asm("x3") = regs[3];
	register unsigned long int x4_ asm("x4") = regs[4];
	register unsigned long int x5_ asm("x5") = regs[5];
	register unsigned long int x6_ asm("x6") = regs[6];
	register unsigned long int sys_id_ asm("x7") = sys_id;
	asm volatile(
		"hvc #0"
		: "+r"(x0_),
		  "+r"(x1_),
		  "+r"(x2_),
		  "+r"(x3_),
		  "+r"(x4_),
		  "+r"(x5_),
		  "+r"(x6_)
		: "r"(sys_id_)
	);
	regs[0] = x0_;
	regs[1] = x1_;
	regs[2] = x2_;
	regs[3] = x3_;
	regs[4] = x4_;
	regs[5] = x5_;
	regs[6] = x6_;
}

static long unlocked_ioctl(struct file *filp, unsigned int cmd, unsigned long argp)
{
	int ret = 0;

	switch (cmd) {
	case ICECAP_VMM_PASSTHRU: {
		struct icecap_vmm_passthru passthru;
		struct icecap_vmm_passthru __user *passthru_user = (struct icecap_vmm_passthru __user *)argp;

		if (copy_from_user(&passthru, (void __user *)passthru_user, sizeof(passthru))) {
			return -EFAULT;
		}

		icecap_vmm_hvc(passthru.sys_id, (unsigned long *)&passthru.regs);

		if (copy_to_user((void __user *)passthru_user, &passthru, sizeof(passthru))) {
			return -EFAULT;
		}
		break;
	}
	default:
		return -EINVAL;
		break;
	}

	return ret;
}

static const struct file_operations fops = {
	.owner = THIS_MODULE,
	.unlocked_ioctl = unlocked_ioctl
};

static int icecap_vmm_init(void)
{
	icecap_vmm_file = debugfs_create_file("icecap_vmm", 0, NULL, NULL, &fops);
	printk("icecap-vmm: ICECAP_VMM_PASSTHRU = 0x%lx", ICECAP_VMM_PASSTHRU);
	return 0;
}

static void icecap_vmm_exit(void)
{
	debugfs_remove(icecap_vmm_file);
}

module_init(icecap_vmm_init)
module_exit(icecap_vmm_exit)
