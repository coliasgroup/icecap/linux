#pragma once

// HACK
// typedef long unsigned int seL4_Word;
typedef long long unsigned int seL4_Word;
typedef seL4_Word seL4_CPtr;
typedef seL4_Word seL4_MessageInfo_t;
typedef seL4_Word seL4_Uint64;

#define seL4_Null 0

#define SEL4_FORCE_LONG_ENUM(type) \
	_enum_pad_ ## type = (1ULL << ((sizeof(long)*8) - 1)) - 1

typedef enum {
	seL4_SysCall = -1,
	seL4_SysReplyRecv = -2,
	seL4_SysSend = -3,
	seL4_SysNBSend = -4,
	seL4_SysRecv = -5,
	seL4_SysReply = -6,
	seL4_SysYield = -7,
	seL4_SysNBRecv = -8,
	seL4_SysDebugPutChar = -9,
	SEL4_FORCE_LONG_ENUM(seL4_Syscall_ID)
} seL4_Syscall_ID;

static inline void arm_sys_send_null(seL4_Word sys, seL4_Word src, seL4_Word info_arg)
{
	register seL4_Word destptr asm("x0") = src;
	register seL4_Word info asm("x1") = info_arg;

	/* Perform the system call. */
	register seL4_Word scno asm("x7") = sys;
	asm volatile(
		"hvc #0"
		: "+r"(destptr), "+r"(info)
		: "r"(scno)
	);
}

static inline void arm_sys_send_recv(seL4_Word sys, seL4_Word dest, seL4_Word *out_badge, seL4_Word info_arg,
									 seL4_Word *out_info, seL4_Word *in_out_mr0, seL4_Word *in_out_mr1, seL4_Word *in_out_mr2, seL4_Word *in_out_mr3,
									 seL4_Word reply)
{
	register seL4_Word destptr asm("x0") = dest;
	register seL4_Word info asm("x1") = info_arg;

	/* Load beginning of the message into registers. */
	register seL4_Word msg0 asm("x2") = *in_out_mr0;
	register seL4_Word msg1 asm("x3") = *in_out_mr1;
	register seL4_Word msg2 asm("x4") = *in_out_mr2;
	register seL4_Word msg3 asm("x5") = *in_out_mr3;

	/* Perform the system call. */
	register seL4_Word scno asm("x7") = sys;
	asm volatile(
		"hvc #0"
		: "+r"(msg0), "+r"(msg1), "+r"(msg2), "+r"(msg3),
		"+r"(info), "+r"(destptr)
		: "r"(scno)
		: "memory"
	);
	*out_info = info;
	*out_badge = destptr;
	*in_out_mr0 = msg0;
	*in_out_mr1 = msg1;
	*in_out_mr2 = msg2;
	*in_out_mr3 = msg3;
}

static inline seL4_MessageInfo_t seL4_MessageInfo_new(seL4_Uint64 label, seL4_Uint64 capsUnwrapped, seL4_Uint64 extraCaps, seL4_Uint64 length)
{
	/* fail if user has passed bits that we will override */
	BUG_ON((label & ~0xfffffffffffffull) != ((0 && (label & (1ull << 63))) ? 0x0 : 0));
	BUG_ON((capsUnwrapped & ~0x7ull) != ((0 && (capsUnwrapped & (1ull << 63))) ? 0x0 : 0));
	BUG_ON((extraCaps & ~0x3ull) != ((0 && (extraCaps & (1ull << 63))) ? 0x0 : 0));
	BUG_ON((length & ~0x7full) != ((0 && (length & (1ull << 63))) ? 0x0 : 0));

	return 0
		| (label & 0xfffffffffffffull) << 12
		| (capsUnwrapped & 0x7ull) << 9
		| (extraCaps & 0x3ull) << 7
		| (length & 0x7full) << 0;
}

static inline seL4_Uint64 seL4_MessageInfo_get_length(seL4_MessageInfo_t seL4_MessageInfo) {
	seL4_Uint64 ret;
	ret = (seL4_MessageInfo & 0x7full) >> 0;
	/* Possibly sign extend */
	if (__builtin_expect(!!(0 && (ret & (1ull << (63)))), 0)) {
		ret |= 0x0;
	}
	return ret;
}

static inline void seL4_DebugPutChar(char c)
{
	seL4_Word unused0 = 0;
	seL4_Word unused1 = 0;
	seL4_Word unused2 = 0;
	seL4_Word unused3 = 0;
	seL4_Word unused4 = 0;
	seL4_Word unused5 = 0;

	arm_sys_send_recv(seL4_SysDebugPutChar, c, &unused0, 0, &unused1, &unused2, &unused3, &unused4, &unused5, 0);
}

static inline void seL4_Signal(seL4_CPtr dest)
{
	arm_sys_send_null(seL4_SysSend, dest, seL4_MessageInfo_new(0, 0, 0, 0));
}

static inline seL4_MessageInfo_t seL4_CallWithMRs(seL4_CPtr dest, seL4_MessageInfo_t msgInfo,
		seL4_Word *mr0, seL4_Word *mr1, seL4_Word *mr2, seL4_Word *mr3)
{
	seL4_MessageInfo_t info;
	seL4_Word msg0 = 0;
	seL4_Word msg1 = 0;
	seL4_Word msg2 = 0;
	seL4_Word msg3 = 0;

	/* Load beginning of the message into registers. */
	if (mr0 != seL4_Null && seL4_MessageInfo_get_length(msgInfo) > 0) {
		msg0 = *mr0;
	}
	if (mr1 != seL4_Null && seL4_MessageInfo_get_length(msgInfo) > 1) {
		msg1 = *mr1;
	}
	if (mr2 != seL4_Null && seL4_MessageInfo_get_length(msgInfo) > 2) {
		msg2 = *mr2;
	}
	if (mr3 != seL4_Null && seL4_MessageInfo_get_length(msgInfo) > 3) {
		msg3 = *mr3;
	}

	arm_sys_send_recv(seL4_SysCall, dest, &dest, msgInfo, &info, &msg0, &msg1, &msg2, &msg3, 0);

	/* Write out the data back to memory. */
	if (mr0 != seL4_Null) {
		*mr0 = msg0;
	}
	if (mr1 != seL4_Null) {
		*mr1 = msg1;
	}
	if (mr2 != seL4_Null) {
		*mr2 = msg2;
	}
	if (mr3 != seL4_Null) {
		*mr3 = msg3;
	}

	return info;
}
